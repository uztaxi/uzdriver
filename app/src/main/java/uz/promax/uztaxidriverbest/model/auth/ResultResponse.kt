package uz.promax.uztaxidriverbest.model.auth

import com.google.gson.annotations.SerializedName

data class ResultResponse(
    @SerializedName("identity")
    val identity: String, // 63000240155438
    @SerializedName("secret")
    val secret: String, // 8Rpl17TmnPDGOLiODDoxrnPIGDKjua9RMFL9Xy8SuBc=
    @SerializedName("owner")
    val owner: String // worker
)