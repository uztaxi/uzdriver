package uz.promax.uztaxidriverbest.model.auth

import com.google.gson.annotations.SerializedName

data class Error(
    @SerializedName("code")
    val code: Int, // -20514
    @SerializedName("message")
    val message: String // error.code.20514
)