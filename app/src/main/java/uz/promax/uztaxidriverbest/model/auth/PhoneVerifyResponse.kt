package uz.promax.uztaxidriverbest.model.auth

import com.google.gson.annotations.SerializedName

data class PhoneVerifyResponse(
    @SerializedName("result")
    val result: ResultResponse?,
    @SerializedName("error")
    val error: Error?
)