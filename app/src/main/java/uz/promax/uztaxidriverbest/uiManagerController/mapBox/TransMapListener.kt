package uz.promax.uztaxidriverbest.uiManagerController.mapBox

import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style

interface TransMapListener {
    fun onTransMapBoxStyle(style: Style,mapboxMap: MapboxMap)
}