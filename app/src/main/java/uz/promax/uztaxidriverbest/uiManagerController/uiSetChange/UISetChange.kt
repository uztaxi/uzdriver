package uz.promax.uztaxidriverbest.uiManagerController.uiSetChange

import android.annotation.SuppressLint
import android.content.ClipboardManager
import android.content.Context.CLIPBOARD_SERVICE
import android.graphics.Color
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.DecelerateInterpolator
import android.view.animation.LinearInterpolator
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.text.isDigitsOnly
import com.airbnb.lottie.LottieAnimationView
import com.google.android.material.card.MaterialCardView
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.romainpiel.shimmer.Shimmer
import com.romainpiel.shimmer.ShimmerTextView
import org.greenrobot.eventbus.EventBus
import uz.promax.gitbranchfull.utils.extentions.logView.toastG
import uz.promax.gitbranchfull.utils.utilsVariable.UtilsVariable.context
import uz.promax.uztaxidriverbest.uiManagerController.mediaPlayer.MediaPlayerController.mediaPlayerDisconnect
import uz.promax.uztaxidriverbest.uiManagerController.mediaPlayer.MediaPlayerController.mediaPlayerOnline
import uz.promax.uztaxidriverbest.uiManagerController.vibratePhone.VibratePhoneManager.vibratePhone
import uz.promax.uztaxidriverbest.utils.interfaceListener.IsOnlineLocation
import java.lang.StringBuilder

object UISetChange {

    private lateinit var blinkanimation: AlphaAnimation

    private val shimmer = Shimmer()

    //card zoom value...
    @SuppressLint("ClickableViewAccessibility")
    fun cardZoomIn(cardZoomIn: MaterialCardView, mapboxMap: MapboxMap) {
        cardZoomIn.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    mapboxMap.animateCamera(CameraUpdateFactory.zoomIn(), 500)
                    cardZoomIn.elevation = 0f
                }
                MotionEvent.ACTION_UP -> {
                    cardZoomIn.elevation = 8f
                }
            }
            true
        }
    }

    //card zoom value...
    @SuppressLint("ClickableViewAccessibility")
    fun cardZoomOut(cardZoomOut: MaterialCardView, mapboxMap: MapboxMap) {
        cardZoomOut.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    mapboxMap.animateCamera(CameraUpdateFactory.zoomOut(), 500)
                    cardZoomOut.elevation = 0f
                }
                MotionEvent.ACTION_UP -> {
                    cardZoomOut.elevation = 8f
                }
            }
            true
        }
    }

    //card gps...
    @SuppressLint("ClickableViewAccessibility")
    fun cardGPS(cardGPS: MaterialCardView, mapboxMap: MapboxMap) {
        cardGPS.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    val eventBus = IsOnlineLocation("online")
                    EventBus.getDefault().post(eventBus)
                    if (mapboxMap.locationComponent.lastKnownLocation != null) {
                        val position = CameraPosition.Builder()
                            .target(
                                LatLng(
                                    mapboxMap.locationComponent.lastKnownLocation!!.latitude,
                                    mapboxMap.locationComponent.lastKnownLocation!!.longitude
                                )
                            )
                            .zoom(16.0)
                            .tilt(45.0)
                            .build()

                        mapboxMap.animateCamera(
                            CameraUpdateFactory.newCameraPosition(position),
                            500
                        )
                    }
                    cardGPS.elevation = 0f
                }
                MotionEvent.ACTION_UP -> {
                    cardGPS.elevation = 8f
                }
            }
            true
        }
    }

    //card block Off...
    @SuppressLint("ClickableViewAccessibility")
    fun cardShiftOff(cardShiftOff: MaterialCardView) {
        cardShiftOff.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    blinkanimation =
                        AlphaAnimation(1f, 0.1f) // Change alpha from fully visible to invisible

                    blinkanimation.duration = 300 // duration

                    blinkanimation.interpolator =
                        LinearInterpolator() // do not alter animation rate

                    blinkanimation.repeatCount = Animation.INFINITE // Repeat animation infinitely

                    blinkanimation.repeatMode = Animation.REVERSE

                    cardShiftOff.startAnimation(blinkanimation)

                    //presenter.turnOffShift()

                    cardShiftOff.elevation = 0f
                }
                MotionEvent.ACTION_UP -> {
                    cardShiftOff.elevation = 8f
                }
            }
            true
        }
    }

    //set uo swiper...
    @SuppressLint("ClickableViewAccessibility")
    fun setUpSwiper(
        rvBlue: RelativeLayout,
        rvGo: RelativeLayout,
        textShimmerStartShift: ShimmerTextView,
        lottieLoader: LottieAnimationView,
        progressBarShift: ProgressBar
    ) {
        rvBlue.alpha = 0f
        rvGo.translationX = 0f
        shimmer.setStartDelay(1333L).setDuration(1233L).start(textShimmerStartShift)
        rvGo.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                when (event!!.action) {
                    MotionEvent.ACTION_DOWN -> {
                        lottieLoader.pauseAnimation()
                    }
                    MotionEvent.ACTION_MOVE -> {
                        if (event.rawX < 768) {
                            rvGo.translationX = event.rawX - 20
                            rvBlue.alpha = event.rawX / 1000.0f
                        }
                    }
                    MotionEvent.ACTION_UP -> {
                        if (event.rawX < 600) {

                            mediaPlayerDisconnect()

                            lottieLoader.resumeAnimation()
                            lottieLoader.playAnimation()
                            lottieLoader.visibility = View.VISIBLE
                            progressBarShift.visibility = View.GONE
                            rvBlue.animate().alpha(0f).setDuration(400).start()
                            rvGo.animate().translationX(0f).setDuration(400)
                                .setInterpolator(DecelerateInterpolator()).start()

                            vibratePhone(50)

                        } else {
                            //presenter.turnOnShift()

                            mediaPlayerOnline()

                            vibratePhone(70)

                            lottieLoader.cancelAnimation()
                            lottieLoader.visibility = View.GONE
                            progressBarShift.visibility = View.VISIBLE
                            rvBlue.animate().alpha(1f).setDuration(400).start()
                            rvGo.animate().translationX(748f).setDuration(400)
                                .setInterpolator(DecelerateInterpolator()).start()


                        }
                    }
                    else -> return false
                }
                return true
            }

        })

    }





}