package uz.promax.uztaxidriverbest.uiManagerController.mapBox

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Location
import android.util.Log
import androidx.core.app.ActivityCompat
import com.mapbox.android.core.location.LocationEngineRequest
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.LocationComponentOptions
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import uz.promax.gitbranchfull.utils.utilsVariable.UtilsVariable.context
import uz.promax.uztaxidriverbest.R

object MapBoxDetails {

    //create map box...
    fun createMapBox(mapView: MapView, transMapListener: TransMapListener) {
        mapView.getMapAsync { mapboxMap ->
            mapboxMap.setStyle(
                Style.Builder().fromUri("mapbox://styles/nurmuhammadandorid/cky49caid2sdl14p6sjt05fky")
            ) { style ->
                mapboxMap.uiSettings.isLogoEnabled = false
                mapboxMap.uiSettings.isTiltGesturesEnabled = false
                mapboxMap.uiSettings.isAttributionEnabled = false
                mapboxMap.uiSettings.isCompassEnabled = false
                mapboxMap.setMaxZoomPreference(18.0)

                val position = CameraPosition.Builder()
                    .target(LatLng(41.31122086155292, 69.27967758784646))
                    .zoom(16.0)
                    .build()

                mapboxMap.moveCamera(CameraUpdateFactory.newCameraPosition(position))
                transMapListener.onTransMapBoxStyle(style, mapboxMap)
            }


        }

    }

    //enabledLocation...
    fun enableLocationComponent(loadedMapStyle: Style, mapboxMap: MapboxMap) {

        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(context)) {

            // Enable the most basic pulsing styling by ONLY using
            // the `.pulseEnabled()` method
            val customLocationComponentOptions = LocationComponentOptions.builder(context)
                .build()


            // Get an instance of the component
            val locationComponent = mapboxMap.locationComponent

            // Activate with options
            locationComponent.activateLocationComponent(
                LocationComponentActivationOptions.builder(context, loadedMapStyle)
                    .locationComponentOptions(customLocationComponentOptions)
                    .useDefaultLocationEngine(true)
                    .locationEngineRequest(
                        LocationEngineRequest.Builder(750)
                            .setFastestInterval(750)
                            .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                            .build()
                    )
                    .build()
            )


            // Enable to make component visible

            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            locationComponent.isLocationComponentEnabled = true


            // Set the component's camera mode
            locationComponent.cameraMode = CameraMode.TRACKING_GPS

            // Set the component's render mode
            locationComponent.renderMode = RenderMode.GPS

        }

    }

    //create map camera update...
    @SuppressLint("Range", "LogNotTimber")
    fun setCameraPosition(location: Location, mapboxMap: MapboxMap) {
        Log.d("@@@", "setCameraPosition: ${location.speed}")
        mapboxMap.animateCamera(
            CameraUpdateFactory.newCameraPosition(
                CameraPosition.Builder()
                    .target(
                        LatLng(
                            location.latitude,
                            location.longitude
                        )
                    )
                    .bearing(location.bearing.toDouble())
                    .zoom(36.0)
                    .tilt(75.0)
                    .build()
            ), 5000
        )
    }

    //add Map image...
    fun addMapImages(mapBoxStyle: Style) {
        mapBoxStyle.addImage(
            "destination_image", Bitmap.createScaledBitmap(
                BitmapFactory.decodeResource(context.resources, R.drawable.destination),
                128, 176,
                true
            )
        )
    }

}