package uz.promax.uztaxidriverbest.uiManagerController.mediaPlayer

import android.media.MediaPlayer
import com.mapbox.mapboxsdk.style.expressions.Expression.raw
import uz.promax.gitbranchfull.utils.utilsVariable.UtilsVariable.context
import uz.promax.uztaxidriverbest.R

object MediaPlayerController {

    lateinit var mp: MediaPlayer


    fun mediaPlayerOnline() {
        mp = MediaPlayer.create(
            context,
            R.raw.online
        )
        mp.start()
    }

    fun mediaPlayerDisconnect() {

        mp = MediaPlayer.create(
            context,
            R.raw.disconnect2
        )

        mp.start()
    }
}