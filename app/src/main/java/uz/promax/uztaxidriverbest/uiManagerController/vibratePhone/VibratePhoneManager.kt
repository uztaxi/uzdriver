package uz.promax.uztaxidriverbest.uiManagerController.vibratePhone

import android.content.Context
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import uz.promax.gitbranchfull.utils.utilsVariable.UtilsVariable.context

object VibratePhoneManager {
    fun vibratePhone(duration: Long) {
        val v = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(
                VibrationEffect.createOneShot(
                    duration,
                    VibrationEffect.EFFECT_TICK
                )
            )
        } else {
            v.vibrate(duration)
        }
    }
}