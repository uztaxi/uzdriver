package uz.tis.kotlinloctionservice.Service

import android.app.*
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.location.Location
import android.os.*
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.android.gms.location.*
import org.greenrobot.eventbus.EventBus
import uz.promax.uztaxidriverbest.R
import uz.tis.kotlinloctionservice.BackgroundLocation


class MyBackgroundService : Service() {

    companion object {
        private val CHANNEL_ID = "channel_01"
        private val PACKAGE_NAME = "uz.tis.kotlinloctionservice.Service"
        private val EXTRA_STARTED_FROM_NOTIFICATION = "$PACKAGE_NAME.started_from_notification"
        private val UPDATE_INTERVAL: Long = 1000
        private val FASTER_UPDATE_INTERVAL: Long = UPDATE_INTERVAL / 2
        private val NOTIFICATION_ID = 1234

    }


    private val binder = LocationBinder()

    inner class LocationBinder : Binder() {
        internal val service: MyBackgroundService
            get() = this@MyBackgroundService
    }

    private var mChangingConfigration = false
    private var mNotificationManager: NotificationManager? = null
    private var locationRequest: LocationRequest? = null
    private var fusedLocationProviderClient: FusedLocationProviderClient? = null
    private var locationCallback: LocationCallback? = null
    private var mServiceHandler: Handler? = null
    private var mLoction: Location? = null

    private val notification: Notification
        get() {
            val intent = Intent(this, MyBackgroundService::class.java)
            val text = CommonService.getLocation(mLoction)
            intent.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true)
            val servicePending =
                PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            val activityPendingIntent =
                PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

            val builder = NotificationCompat.Builder(this)
                .addAction(R.drawable.ic_launcher_foreground, "Launch", activityPendingIntent)
                .addAction(R.drawable.ic_launcher_foreground, "Cancel", servicePending)
                .setContentText(text)
                .setContentTitle(CommonService.getLocationTitle(this))
                .setOngoing(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker(text)
                .setWhen(System.currentTimeMillis())

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                builder.setChannelId(CHANNEL_ID)
            return builder.build()
        }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val startedFromNotification =
            intent!!.getBooleanExtra(EXTRA_STARTED_FROM_NOTIFICATION, false)
        if (startedFromNotification) {
            removeLocationUpdate()
            stopSelf()
        }
        return Service.START_NOT_STICKY
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        mChangingConfigration = true
    }

    fun removeLocationUpdate() {
        try {
            fusedLocationProviderClient!!.removeLocationUpdates(locationCallback!!)
            CommonService.setRequestingLocationUpdates(this, false)
            stopSelf()

        } catch (ex: SecurityException) {
            CommonService.setRequestingLocationUpdates(this, true)
        }
    }

    override fun onCreate() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                super.onLocationResult(p0)
                onNewLocation(p0!!.lastLocation)
            }
        }
        createLocationRequest()
        getLastLocation()

        val hamdlerThread = HandlerThread("NUR")
        hamdlerThread.start()
        mServiceHandler = Handler(hamdlerThread.looper)
        mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = packageName
            val mChannel =
                NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT)
            mNotificationManager!!.createNotificationChannel(mChannel)
        }
    }

    private fun getLastLocation() {
        try {
            fusedLocationProviderClient!!.lastLocation.addOnCompleteListener { task ->
                if (task.isSuccessful && task.result != null) {
                    mLoction = task.result

                } else
                    Log.d("@@@", "getLastLocation: Faeild to get location...")
            }
        } catch (ex: SecurityException) {
            Log.d("@@@", "getLastLocation: Faild to get location...${ex.message}")
        }
    }

    private fun createLocationRequest() {
        locationRequest = LocationRequest()
        locationRequest!!.interval = UPDATE_INTERVAL
        locationRequest!!.fastestInterval = FASTER_UPDATE_INTERVAL
        locationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private fun onNewLocation(lastLocation: Location) {
        mLoction = lastLocation
        EventBus.getDefault().postSticky(BackgroundLocation(mLoction!!))
        if (setviceIsRunningForegraund(this)) {
            mNotificationManager!!.notify(NOTIFICATION_ID, notification)
        }
    }

    private fun setviceIsRunningForegraund(context: Context): Boolean {
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (javaClass.name.equals(service.service.className))
                if (service.foreground)
                    return true
        }
        return false
    }

    override fun onBind(intent: Intent): IBinder {
        stopForeground(true)
        mChangingConfigration = false
        return binder
    }

    override fun onRebind(intent: Intent?) {
        stopForeground(false)
        mChangingConfigration = false
        super.onRebind(intent)
    }

    override fun onUnbind(intent: Intent?): Boolean {
        if (!mChangingConfigration && CommonService.RequestingLocationUpdates(this)) {
            startForeground(NOTIFICATION_ID, notification)
        }
        return true
    }

    override fun onDestroy() {
        mServiceHandler!!.removeCallbacksAndMessages(null)
        super.onDestroy()
    }

    fun requestLocationUpdate() {
        CommonService.setRequestingLocationUpdates(this, true)
        startService(Intent(applicationContext, MyBackgroundService::class.java))
        try {
            fusedLocationProviderClient!!.requestLocationUpdates(
                locationRequest!!,
                locationCallback!!,
                Looper.myLooper()!!
            )
        } catch (ex: SecurityException) {
            CommonService.setRequestingLocationUpdates(this, false)
            Log.d("@@@", "requestLocationUpdate: $ex")
        }
    }
}