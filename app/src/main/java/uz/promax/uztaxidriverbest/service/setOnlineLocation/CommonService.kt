package uz.tis.kotlinloctionservice.Service

import android.content.Context
import android.location.Location
import android.preference.PreferenceManager
import java.text.DateFormat
import java.util.*

object CommonService {
     val KEY_REQUEST_LOCATION_UPDATE = "location"
    fun getLocation(location: Location?): String {
        return if (location == null) {
            "Unkown location"
        } else {
            "${location.longitude} ${location.latitude}"
        }
    }

    fun getLocationTitle(context: Context): String {
        return String.format("Location update $1$", DateFormat.getDateInstance().format(Date()))
    }

    fun setRequestingLocationUpdates(context: Context, b: Boolean) {
        PreferenceManager.getDefaultSharedPreferences(context)
            .edit()
            .putBoolean(KEY_REQUEST_LOCATION_UPDATE, b)
            .apply()
    }

    fun RequestingLocationUpdates(context: Context): Boolean {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(
            KEY_REQUEST_LOCATION_UPDATE, false
        )
    }
}