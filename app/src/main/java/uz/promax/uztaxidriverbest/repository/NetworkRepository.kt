package uz.promax.uztaxidriverbest.repository

import uz.promax.uztaxidriverbest.api.ApiService
import javax.inject.Inject

class NetworkRepository @Inject constructor(private val apiService: ApiService) {

    /*******************AUTH**LOGIN***************************************/
    /**get code*/
    suspend fun createUser(code: String) = apiService.authWithSecretCode(code)
}