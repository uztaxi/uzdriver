package uz.promax.uztaxidriverbest

import android.content.*
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.view.WindowManager
import kotlinx.android.synthetic.main.activity_main.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import uz.promax.gitbranchfull.utils.extentions.logView.toastG
import uz.promax.gitbranchfull.utils.runTimePermission.OnRunTimePermissionListener
import uz.promax.gitbranchfull.utils.runTimePermission.RunTimePermission
import uz.promax.uztaxidriverbest.databinding.ActivityMainBinding
import uz.promax.uztaxidriverbest.socket.SocketManager
import uz.promax.uztaxidriverbest.utils.interfaceListener.IsOnlineLocation
import uz.tis.kotlinloctionservice.BackgroundLocation
import uz.tis.kotlinloctionservice.Service.CommonService
import uz.tis.kotlinloctionservice.Service.MyBackgroundService

class MainActivity : AppCompatActivity(), SharedPreferences.OnSharedPreferenceChangeListener,
    OnRunTimePermissionListener {

    private lateinit var binding: ActivityMainBinding
    private var mService: MyBackgroundService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                window.decorView.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
            }
        }

        window.statusBarColor = Color.TRANSPARENT

        val permission = RunTimePermission()
        permission.permissionList(this)

    }


    /*******************************************************************************/
    //background service create....
    private var mBound = false

    private val mServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
            val binder = p1 as MyBackgroundService.LocationBinder
            mService = binder.service
            mBound = true
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            mService = null
            mBound = false
        }

    }

    //socketga daniylarni berib yuborish...
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onBackgroundLocationService(event: BackgroundLocation) {
        if (event.location != null) {
            toastG(event.location.toString())
        }
    }

    //isOnline на линии
    @Subscribe
    fun onMessageEvent(isOnline: IsOnlineLocation) {
        if (isOnline.isOnline == "online") {
            mService!!.requestLocationUpdate()
        } else {
            mService!!.removeLocationUpdate()
        }
    }

    override fun onSharedPreferenceChanged(p0: SharedPreferences?, p1: String?) {
        if (p1.equals(CommonService.KEY_REQUEST_LOCATION_UPDATE)) {
            setButtonState(p0!!.getBoolean(CommonService.KEY_REQUEST_LOCATION_UPDATE, false))
        }
    }

    private fun setButtonState(boolean: Boolean) {

    }

    /*******************************************************************************/

    override fun onPermissionGranted() {
        bindService(
            Intent(this@MainActivity, MyBackgroundService::class.java),
            mServiceConnection,
            Context.BIND_AUTO_CREATE
        )

        mService!!.requestLocationUpdate()
    }

    override fun onPermissionDenied() {

    }

    override fun onStart() {
        super.onStart()

        PreferenceManager.getDefaultSharedPreferences(this)
            .registerOnSharedPreferenceChangeListener(this)

        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()

        PreferenceManager.getDefaultSharedPreferences(this)
            .unregisterOnSharedPreferenceChangeListener(this)

        EventBus.getDefault().unregister(this)
    }
}