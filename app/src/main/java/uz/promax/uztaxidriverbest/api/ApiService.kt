package uz.promax.uztaxidriverbest.api

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import uz.promax.uztaxidriverbest.model.auth.PhoneVerifyResponse

interface ApiService {

    @GET("driver-app/1.1/profile/verify")
    suspend fun authWithSecretCode(@Query("code") secretCode: String): Response<PhoneVerifyResponse>

}