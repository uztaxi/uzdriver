package uz.promax.gitbranchfull.utils.extentions.dateFormat

import android.os.Build
import androidx.annotation.RequiresApi
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

/* local date get */
@RequiresApi(Build.VERSION_CODES.O)
fun nowDate(): String {
    val current = LocalDateTime.now()
    val formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
    return current.format(formatter)  //Aug 2, 2017 11:44:19 AM
}

/* now date beautiful */
@RequiresApi(Build.VERSION_CODES.O)
fun nowDateBeautiful(): String {
    val current = LocalDateTime.now()
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")
    return current.format(formatter)  //2017-08-02 11:29:57.401
}


/* now date beautiful */
@RequiresApi(Build.VERSION_CODES.O)
fun nowDateBeautifulOne(): String {
    val current = LocalDateTime.now()
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    return current.format(formatter)  //2017-08-02
}

/* convert seconds */
@RequiresApi(Build.VERSION_CODES.O)
fun convertDateSeconds(): String {
    val current = LocalDateTime.now()
    val formatter = DateTimeFormatter.BASIC_ISO_DATE
    return current.format(formatter)  //20170802
}

/* convert string to date */
@RequiresApi(Build.VERSION_CODES.O)
fun convertStringToDate(date: String): LocalDate? {
    return LocalDate.parse(date, DateTimeFormatter.ISO_DATE)
}