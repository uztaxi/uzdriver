package uz.promax.gitbranchfull.utils.transSimple

import uz.promax.gitbranchfull.utils.utilsVariable.UtilsVariable.context

class SimpleTrans {
    private var itemClickListener: ((Int) -> Unit)? = null



    fun setItemClickListener(f: (id: Int) -> Unit) {
        itemClickListener = f


    }

    //author fun ...
    fun onItemClickListener(){
        itemClickListener?.invoke(0)
    }
}