package uz.promax.gitbranchfull.utils.errorHandler

import android.util.Log

class ErrorHandlerDemo : Thread.UncaughtExceptionHandler  {
    override fun uncaughtException(p0: Thread, ex: Throwable) {
        Log.e("ERROR", "uncaughtException: ${ex.message}")
    }
}