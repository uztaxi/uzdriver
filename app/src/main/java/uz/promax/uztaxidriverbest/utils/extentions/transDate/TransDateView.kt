package uz.promax.gitbranchfull.utils.extentions.transDate

import android.os.Bundle

fun transBundle(key: String, value: String): Bundle {
    val bundle = Bundle()
    bundle.putString(key, value)
    return bundle
}

fun transBundleList(mapsList: HashMap<String, String>): Bundle {
    val bundle = Bundle()
    for(key in mapsList.keys){
        println("Element at key $key = ${mapsList[key]}")
        bundle.putString(key,mapsList[key])
    }

    return bundle
}

