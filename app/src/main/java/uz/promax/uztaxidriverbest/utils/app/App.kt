package uz.promax.gitbranchfull.utils.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import uz.promax.gitbranchfull.utils.utilsVariable.UtilsVariable

@HiltAndroidApp
class App : Application() {

    override fun onCreate() {
        super.onCreate()

        UtilsVariable.context = this

    }
}