package uz.promax.gitbranchfull.utils.extentions.logicFun

import android.util.TypedValue
import uz.promax.gitbranchfull.utils.sharedPref.SharedPref
import uz.promax.gitbranchfull.utils.utilsVariable.UtilsVariable.context

//logic and beautiful fun...

/*convert to card */
fun changeFormatCard(cardNumber: String): String {
    val result = StringBuilder()
    for (i in 0 until cardNumber.lastIndex) {
        if (i % 4 == 0 && i != 0) {
            result.append(" ")
        }
        result.append(cardNumber[i])
    }
    return result.toString()
}

/* convert to card * beautiful*/
fun convertCardBeautiful(cardNumber: String): String {
    val builder = StringBuilder()
    builder.append(cardNumber.subSequence(0, 4))
    builder.append(" ")
    builder.append(cardNumber.subSequence(4, 6))
    builder.append("** **** ")
    builder.append(cardNumber.subSequence(cardNumber.length - 4, cardNumber.length))
    return builder.toString()
}

/* convert to number...*/
fun convertToPhone(convertPhone: String): String {
    //998993018716 to +998 99 301 87 16
    val phoneBuilder = StringBuilder()
    phoneBuilder.append("+")
    for (i in convertPhone.indices) {
        if (i == 3 || i == 5 || i == 8 || i == 10) {
            phoneBuilder.append(" ")
        }
        phoneBuilder.append(convertPhone[i])
    }
    return phoneBuilder.toString()
}

/* convert to money */
fun convertMoney(convertMoney: String): String {
    var result = ""
    var sNum = convertMoney

    for (i in 1..sNum.length / 3) {
        result = "${sNum.substring(sNum.length - 3)} $result"
        sNum = sNum.substring(0, sNum.length - 3)
    }

    return if (sNum.isNotEmpty()) "$sNum $result " else result
}

/* convert to float for dp */
fun convertFloatDp(value: Float): Float {
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        value,
        context.resources.displayMetrics
    )
}

/* end point lang added for multi lang */
fun langPrice(): String {
    return when (SharedPref.getLanguage()) {
        "uz" -> {
            "sum"
        }
        "ru" -> {
            "Сум"
        }
        else -> {
            "price"
        }
    }
}