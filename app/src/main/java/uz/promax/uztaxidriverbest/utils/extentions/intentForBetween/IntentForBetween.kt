package uz.promax.gitbranchfull.utils.extentions.intentForBetween

import android.content.Intent
import android.net.Uri
import uz.promax.gitbranchfull.utils.utilsVariable.UtilsVariable.context

/* intent call page*/
fun intentCallPage(phone: String) {
    val callIntent = Intent(Intent.ACTION_CALL)
    callIntent.data = Uri.parse("tel:$phone")
    context.startActivity(callIntent)
}

/* intent geo location setting page*/
fun intentGeoLocation() {
    context.startActivity(Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS))
}