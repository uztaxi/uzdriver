package uz.promax.gitbranchfull.utils.runTimePermission

import android.Manifest
import android.content.Context
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.single.PermissionListener
import uz.promax.englishlearning.utils.runTimePermission.OnRunTimeReadStorage
import uz.promax.gitbranchfull.utils.utilsVariable.UtilsVariable.context

/* run time permission class*/
class RunTimePermission() {

    //if you run time per need Manifest file write you names!!

    //for camera permission...
    fun cameraPermission(onRunTimeReadStorage: OnRunTimeReadStorage) {
        Dexter.withContext(context)
            .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse) {
                    onRunTimeReadStorage.onRunTimeReadStorageListener(true)
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse) {
                    onRunTimeReadStorage.onRunTimeReadStorageListener(false)
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest,
                    token: PermissionToken
                ) {
                }
            }).check()
    }

    //fun permission list....
    fun permissionList(onRunTimePermissionListener: OnRunTimePermissionListener) {
        Dexter.withContext(context)
            .withPermissions(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.CALL_PHONE,
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    onRunTimePermissionListener.onPermissionGranted()
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest?>?,
                    token: PermissionToken?
                ) {
                    onRunTimePermissionListener.onPermissionDenied()
                }
            }).check()
    }

}