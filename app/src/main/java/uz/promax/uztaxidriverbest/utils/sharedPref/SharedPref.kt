package uz.promax.gitbranchfull.utils.sharedPref

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import uz.promax.gitbranchfull.utils.utilsVariable.UtilsVariable.context

object SharedPref {

    /**change lang*/
    @SuppressLint("ApplySharedPref")
    fun changLanguage(lang: String) {
        val sharedPreferences = context.getSharedPreferences("Lang", Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString("lang", lang).commit()
    }

    fun getLanguage(): String? {
        val sharedPreferences = context.getSharedPreferences("Lang", Context.MODE_PRIVATE)
        return sharedPreferences.getString("lang", "ru")
    }


    /**change test count*/
    @SuppressLint("ApplySharedPref")
    fun changTestCount(count: Int) {
        val sharedPreferences = context.getSharedPreferences("Count", Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putInt("count", count).commit()
    }

    fun getCount(): Int {
        val sharedPreferences = context.getSharedPreferences("Count", Context.MODE_PRIVATE)
        return sharedPreferences.getInt("count", 15)
    }

    @SuppressLint("ApplySharedPref")
    fun insertDictionary(title: String) {
        val sharedPreferences = context.getSharedPreferences("Dictionary", Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString("title", title).commit()
    }

    fun getDictionaryModel(): String? {
        val sharedPreferences = context.getSharedPreferences("Dictionary", Context.MODE_PRIVATE)
        return sharedPreferences.getString("title", "")
    }


    @SuppressLint("ApplySharedPref")
    fun insertToken(token: String) {
        val sharedPreferences = context.getSharedPreferences("Token", Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString("token", token).commit()
    }

    fun getTokenModel(): String? {
        val sharedPreferences = context.getSharedPreferences("Token", Context.MODE_PRIVATE)
        return sharedPreferences.getString("token", "")
    }
}