package uz.promax.englishlearning.utils.runTimePermission

interface OnRunTimeReadStorage {
    fun onRunTimeReadStorageListener(value: Boolean)
}