package uz.promax.uztaxidriverbest.utils

class SocketUtils {

    companion object{
        const val LOGIN = "login"
        const val DATA = "data"
        const val BALANCE = "balance"
        const val AUTO_OFFER = "auto_offer"
        const val START_SHIFT = "start-shift"
        const val END_SHIFT = "end-shift"
        const val ACTIVE_PLANS ="active_plans"
        const val U_ORDERS ="u-orders"
        const val ACCOUNT_UPDATE = "account_update"
        const val ACTIVATE_PLAN = "activate-plan"
        const val DEACTIVATE_PLAN = "deactivate-plan"
        const val WAY_ORDER = "way_order"
        const val PLANS = "plans"
        const val GET_GEOMETRY = "get_geometry"
        const val FOR_TAXIMETER_SEND = "for-taximeter"
        const val FOR_TAXIMETER_RECEIVE = "for_taximeter"
        const val FREE_MODE_CHANGE = "discharging"
        const val TAXIMETER_DATA_SEND = "reestimate"
        const val CALCULATE_ORDER = "calculate_order"
        const val COMPLETE_ORDER = "complete_order"
        const val EXPIRED_ORDER = "expired_order"
        const val NOTIFY = "notify"
        const val OTHER = "other"
        const val ACCEPT_OFFER = "accept-offer"
        const val TRY_ORDER = "try_order"
        const val DECLINCE_OFFER = "decline-offer"
        const val ARRIVED_ORDER = "arrived_order"
        const val ORDER_ASSIGNED = "driver.assigned"
        const val CONFIG_CHANGES = "config"
    }

}