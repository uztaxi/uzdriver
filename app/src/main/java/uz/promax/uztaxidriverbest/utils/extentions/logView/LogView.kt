package uz.promax.gitbranchfull.utils.extentions.logView

import android.util.Log
import android.widget.Toast
import uz.promax.gitbranchfull.utils.utilsVariable.UtilsVariable

/* toast */
fun toastG(value: String) {
    Toast.makeText(UtilsVariable.context, value, Toast.LENGTH_SHORT).show()
}

/* log */
fun log(key:String,value:String){
    Log.d(key, value)
}