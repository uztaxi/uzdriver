package uz.promax.uztaxidriverbest.socket

import android.annotation.SuppressLint
import android.util.Log
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.emitter.Emitter
import io.socket.engineio.client.transports.WebSocket
import okhttp3.OkHttpClient
import uz.promax.uztaxidriverbest.utils.Constants
import uz.promax.uztaxidriverbest.utils.SocketUtils

object SocketManager {

    private var TAG = "SocketManager"

    private lateinit var socket: Socket

    init {
        SocketHandler.setSocket()
        SocketHandler.establishConnection()
        socket = SocketHandler.getSocket()

        Log.d(TAG, "init ")
    }

    @SuppressLint("LogNotTimber")
    fun setUpDriverSocket() {
        Log.d(TAG, "setUpDriverSocket: ${socket.connected()}")
    }
}