package uz.promax.uztaxidriverbest.socket


import io.socket.client.IO
import io.socket.client.Socket
import io.socket.engineio.client.transports.WebSocket
import okhttp3.OkHttpClient
import java.net.URISyntaxException

object SocketHandler {

    lateinit var mSocket: Socket

    @Synchronized
    fun setSocket() {
        try {
            val options = IO.Options()
            options.forceNew = true
            options.transports = arrayOf(WebSocket.NAME)
            options.reconnectionAttempts = 3
            options.reconnection = true
            options.timeout = 2000
            options.path = "/api"

            val okHttpClient: OkHttpClient = OkHttpClient.Builder().build()

            IO.setDefaultOkHttpWebSocketFactory(okHttpClient)
            IO.setDefaultOkHttpCallFactory(okHttpClient)

            options.apply {
                callFactory = okHttpClient
                webSocketFactory = okHttpClient
            }

            mSocket = IO.socket("http://188.120.232.38:3100")
        } catch (e: URISyntaxException) {

        }
    }

    @Synchronized
    fun getSocket(): Socket {
        return mSocket
    }

    @Synchronized
    fun establishConnection() {
        mSocket.connect()
    }

    @Synchronized
    fun closeConnection() {
        mSocket.disconnect()
    }
}