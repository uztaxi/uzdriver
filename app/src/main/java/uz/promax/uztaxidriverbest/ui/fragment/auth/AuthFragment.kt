package uz.promax.uztaxidriverbest.ui.fragment.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import dagger.hilt.android.AndroidEntryPoint
import uz.promax.uztaxidriverbest.databinding.FragmentAuthBinding

@AndroidEntryPoint
class AuthFragment : Fragment(), View.OnClickListener {
    private var _binding: FragmentAuthBinding? = null
    private val binding get() = requireNotNull(_binding)
    private lateinit var authUISetPresenter: AuthUISetPresenter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentAuthBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        authUISetPresenter = AuthUISetPresenter()

        uiOnItemClickListener()

    }

    private fun uiOnItemClickListener() {
        binding.textViewNumber1.setOnClickListener(this)
        binding.textViewNumber2.setOnClickListener(this)
        binding.textViewNumber3.setOnClickListener(this)
        binding.textViewNumber4.setOnClickListener(this)
        binding.textViewNumber5.setOnClickListener(this)
        binding.textViewNumber6.setOnClickListener(this)
        binding.textViewNumber7.setOnClickListener(this)
        binding.textViewNumber8.setOnClickListener(this)
        binding.textViewNumber9.setOnClickListener(this)
        binding.textViewNumber11.setOnClickListener(this)
        binding.textViewNumber12.setOnClickListener {
            authUISetPresenter.backDriverSecretKey(binding.textView12Digit)
        }
        binding.imageViewPaste.setOnClickListener {
            authUISetPresenter.viewPaste(binding.textView12Digit, binding.textViewError)
        }

        binding.btnSendCode.setOnClickListener {
            binding.textSendCode.visibility = View.GONE
            binding.progressBar.visibility = View.VISIBLE
        }


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onClick(view: View?) {
        val btn = view as TextView
        authUISetPresenter.authSecretKey(btn, binding.textView12Digit)
    }


}