package uz.promax.uztaxidriverbest.ui.fragment.auth

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import uz.promax.uztaxidriverbest.model.auth.ResultResponse
import uz.promax.uztaxidriverbest.repository.NetworkRepository
import javax.inject.Inject

@HiltViewModel
class AuthViewModel @Inject constructor(private val networkRepository: NetworkRepository) :
    ViewModel() {

    val phoneResult = MutableLiveData<ResultResponse>()

    val error = MutableLiveData<String>()
    fun createUser(number: String) = viewModelScope.launch {
        try {
            networkRepository.createUser(number).let {
                if (it.isSuccessful) {
                    if (it.body()!!.error == null) {
                        phoneResult.postValue(it.body()!!.result!!)
                    } else {
                        error.postValue(it.body()!!.error!!.message)
                    }
                }
            }
        } catch (ex: Exception) {
            error.postValue("${ex.message} ")
        }
    }
}