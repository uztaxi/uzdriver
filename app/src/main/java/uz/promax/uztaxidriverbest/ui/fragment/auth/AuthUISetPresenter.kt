package uz.promax.uztaxidriverbest.ui.fragment.auth

import android.content.ClipboardManager
import android.content.Context
import android.graphics.Color
import android.widget.TextView
import androidx.core.text.isDigitsOnly
import uz.promax.gitbranchfull.utils.utilsVariable.UtilsVariable
import java.lang.StringBuilder

class AuthUISetPresenter {

    /*****************->auth fragment set ui<-***************************************/
    //driver secretKey builder...
    var driverSecretKey = StringBuilder()

    //driver secret key logic...
    fun authSecretKey(textView: TextView, setText: TextView) {
        if (driverSecretKey.length != 12) {
            driverSecretKey.append(textView.text.toString())
            writeTextDIGIT(setText)
        }
    }

    //back btn for key...
    fun backDriverSecretKey(setText: TextView) {
        if (driverSecretKey.isNotEmpty()) {
            driverSecretKey.setLength(driverSecretKey.length - 1)
            writeTextDIGIT(setText)
        }
    }

    //view paste...
    fun viewPaste(setText: TextView, textViewError: TextView) {
        val clipboard: ClipboardManager =
            UtilsVariable.context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val textCopied = clipboard.text.toString()
        if (textCopied.isDigitsOnly() && textCopied.length == 12 || textCopied.length == 14) {
            driverSecretKey.clear()
            driverSecretKey.append(textCopied.replace(" ", ""))
            writeTextDIGIT(setText)
            textViewError.text = ""
        } else {
            textViewError.text =
                "You haven't copied 12 Digit number! You pasted (${textCopied})"
        }
    }

    //set textView ui for secret key...
    private fun writeTextDIGIT(textSetView: TextView) {
        if (driverSecretKey.isNotEmpty()) {
            textSetView.setTextColor(Color.BLACK)
            val s = StringBuilder(driverSecretKey.toString())

            var i = 4
            while (i < s.length) {
                s.insert(i, " ")
                i += 5
            }

            textSetView.text = s
        } else {
            textSetView.setTextColor(Color.parseColor("#A1A9CA"))
            textSetView.text = "Enter 12 digit number"
        }
    }
}