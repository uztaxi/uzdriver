package uz.promax.uztaxidriverbest.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import uz.promax.gitbranchfull.utils.extentions.logView.toastG
import uz.promax.uztaxidriverbest.R
import uz.promax.uztaxidriverbest.databinding.FragmentHomeBinding
import uz.promax.uztaxidriverbest.uiManagerController.mapBox.MapBoxDetails.addMapImages
import uz.promax.uztaxidriverbest.uiManagerController.mapBox.MapBoxDetails.createMapBox
import uz.promax.uztaxidriverbest.uiManagerController.mapBox.MapBoxDetails.enableLocationComponent
import uz.promax.uztaxidriverbest.uiManagerController.mapBox.MapBoxDetails.setCameraPosition
import uz.promax.uztaxidriverbest.uiManagerController.mapBox.TransMapListener
import uz.promax.uztaxidriverbest.uiManagerController.uiSetChange.UISetChange.cardGPS
import uz.promax.uztaxidriverbest.uiManagerController.uiSetChange.UISetChange.cardShiftOff
import uz.promax.uztaxidriverbest.uiManagerController.uiSetChange.UISetChange.cardZoomIn
import uz.promax.uztaxidriverbest.uiManagerController.uiSetChange.UISetChange.cardZoomOut
import uz.promax.uztaxidriverbest.uiManagerController.uiSetChange.UISetChange.setUpSwiper
import uz.tis.kotlinloctionservice.BackgroundLocation

class HomeFragment : Fragment(), TransMapListener {
    private var _binding: FragmentHomeBinding? = null
    private lateinit var mapboxMap: MapboxMap
    private var savedInstanceState: Bundle? = null
    private lateinit var mapBoxStyle: Style
    private val binding get() = requireNotNull(_binding)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment

        Mapbox.getInstance(requireContext(), getString(R.string.mapbox_access_token))
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        createMapView()

        uiOnItemClickListener()
    }

    private fun uiOnItemClickListener() {
//        binding.btn.setOnClickListener {
//            val eventBus = IsOnlineLocation("online")
//            EventBus.getDefault().post(eventBus)
//        }

        binding.cardOffers.setOnClickListener {
            findNavController().navigate(R.id.action_fragment_map_to_fragment_auth)
        }
    }

    /*******Create mapBox***************************************************************/
    private fun createMapView() {

        binding.mapView.onCreate(savedInstanceState)
        //create mapBox...
        createMapBox(binding.mapView, this)

    }

    override fun onTransMapBoxStyle(style: Style, mapboxMap: MapboxMap) {
        try {
            this.mapboxMap = mapboxMap
            mapBoxStyle = style
            setUpMapButtons()
            addMapImages(mapBoxStyle)
        } catch (ex: Exception) {
            toastG(ex.message.toString())
        }
    }

    //socketga daniylarni berib yuborish...
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onBackgroundLocationService(event: BackgroundLocation) {
        if (event.location != null) {
            setCameraPosition(event.location!!, mapboxMap)
        }
    }

    /**********************************************************************************/
    //create set up btn...
    private fun setUpMapButtons() {
        enableLocationComponent(mapBoxStyle, mapboxMap)

        cardZoomIn(binding.cardZoomIn, mapboxMap)

        cardZoomOut(binding.cardZoomOut, mapboxMap)

        cardGPS(binding.cardGPS, mapboxMap)

        cardShiftOff(binding.cardShiftOff)

        setUpSwiper(binding.containerShift.rvBlue, binding.containerShift.rvGo, binding.containerShift.textShimmerStartShift, binding.containerShift.lottieLoader, binding.containerShift.progressBarShift)
    }

    /**life sicle *******************************************/
    override fun onStart() {
        super.onStart()
        binding.mapView.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onResume() {
        super.onResume()
        binding.mapView.onResume()

    }

    override fun onPause() {
        super.onPause()
        binding.mapView.onPause()

    }

    override fun onStop() {
        super.onStop()
        binding.mapView.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}
